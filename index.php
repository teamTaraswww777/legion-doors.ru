<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
?>
<div class="intro-index">

        <div class="big-bg">
            <img src="<?= SITE_TEMPLATE_PATH?>/img/Bg1.png" alt="">
        </div>

        <div class="container-frame">

            <div class="frame">


                <h1>
                    <span class="fr-lef">ОТКРОЙ <br> В МИР</span>
                    <span class="fr-rig">ДВЕРЬ <br>НАДЕЖНОСТИ</span>
                </h1>

                <div id="the-door-wrap" class="the-door-wrap abs abs-mid">
                    <a href="/catalog/" id="the-door" class="the-door abs"></a>
                    <a href="/catalog/" id="door-2" class="door-2 abs"></a>
                    <img class="door-frame abs" src="<?= SITE_TEMPLATE_PATH?>/img/frame.png" alt="">
                    <img class="door-top-beam abs" src="<?= SITE_TEMPLATE_PATH?>/img/door-top-beam.png" alt="">
                    <img class="light-beam abs" src="<?= SITE_TEMPLATE_PATH?>/img/light-beam.png" alt="">
                </div>


            </div>

        </div>

        <div class="big-bg">
            <img src="<?= SITE_TEMPLATE_PATH?>/img/Bg2.png" alt="">
        </div>

    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>