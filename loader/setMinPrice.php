<? die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Loader,
    \Bitrix\Iblock\SectionTable;

Loader::includeModule('iblock');

$sections = SectionTable::getList(
    [
        'filter' => [
            'IBLOCK_ID' => 5,
            'ACTIVE' => 'Y'
        ],
        'select' => [
            '*'
        ]
    ]
)->fetchAll();

foreach ($sections as $section) {
    $minPrice = 0;
    $elementsRes = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => 5,
            'SECTION_ID' => $section['ID']
        ],
        false,
        false,
        [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_PRICE'
        ]
    );
    $elementNumber = 0;
    while ($element = $elementsRes->GetNext()) {
        if ($elementNumber === 0 && intval($element['PROPERTY_PRICE_VALUE']) > 0) {
            $minPrice = intval($element['PROPERTY_PRICE_VALUE']);
        } else {
            if ($minPrice > intval($element['PROPERTY_PRICE_VALUE'])) {
                $minPrice = intval($element['PROPERTY_PRICE_VALUE']);
            }
        }
        $elementNumber++;
    }
    $sect = new CIBlockSection;
    $fields = [
        'UF_MIN_PRICE' => $minPrice
    ];
    $res = $sect->Update(
        $section['ID'],
        $fields
    );
}