<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Zend\Dom\Query,
    \Bitrix\Main\Loader;

Loader::includeModule('iblock');

$sourcePage = file_get_contents('https://dveriklin.ru/catalog/tselnognutye-dveri/trehkonturnye-dveri/');
$zendDocument = new Query($sourcePage);

$doorsLinks = $zendDocument->execute('.items a');
foreach ($doorsLinks as $doorsLink) {
    $doorPage = file_get_contents('https://dveriklin.ru' . $doorsLink->getAttribute('href'));
    $zendData = new Query($doorPage);
    $name = $zendData->execute('.name_price_btn h1')[0]->textContent;
    $nameExplode = explode(' ', trim($name));
    $modelName = $nameExplode[count($nameExplode) - 1];
    $modelExplode = explode('-', $modelName);
    $newModelName = strrev($modelExplode[0]) . '-' . $modelExplode[1];
    $name = str_replace($modelName, $newModelName, $name);
    $photo = CFile::MakeFileArray('https://dveriklin.ru' . $zendData->execute('.item .img_element img')[0]->getAttribute('src'));
    $techPhoto = CFile::MakeFileArray('https://dveriklin.ru' . $zendData->execute('.door-contet .dop-pic-char img')[0]->getAttribute('src'));
    $price = intval(explode(' ', trim($zendData->execute('.name_price_btn .price')[0]->textContent))[count(explode(' ', trim($zendData->execute('.name_price_btn .price')[0]->textContent))) - 2]) * 0.82;
    $code = CUtil::translit(
        $name,
        'ru',
        [
            "replace_space" => "-",
            "replace_other" => "-"
        ]
    );
    $element = new CIBlockElement;
    $doorData = [
        'IBLOCK_ID' => 5,
        'IBLOCK_SECTION_ID' => 24,
        'NAME' => $name,
        'CODE' => $code,
        'PREVIEW_PICTURE' => $photo,
        'DETAIL_PICTURE' => $photo,
        'PROPERTY_VALUES' => [
            'PRICE' => $price,
            'TECH_PICTURE' => $techPhoto
        ]
    ];
    $res = $element->Add($doorData);
}