$(function() {

    let header = $("#header");
    let intro = $("#intro");
    let introH = intro.innerHeight();
    let scrollPos = $(window).scrollTop();

    $(window).on("scroll load resize", function() {
        introH = intro.innerHeight();
        scrollPos = $(this).scrollTop();

        if(scrollPos > introH) {
            header. addClass("fixed");
        } else {
            header. removeClass("fixed");
        }

    });


    $('body').removeClass('nojs');
    $("img, a").on("dragstart", function(event) { event.preventDefault(); });
    $('.range').rangeslider({
        polyfill: false,
    });

    $(".toggle-mnu").click(function() {
        $(this).toggleClass("on");
        $(".nav").slideToggle();
        return false;
    });



    $(document).on('ready', function() {

        $(".sliders").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
    });


    });

/*var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
    output.innerHTML = this.value;
};

var slideCol = document.getElementById("Range");
var y = document.getElementById("demee");
y.innerHTML = slideCol.value;

slideCol.oninput = function() {
    y.innerHTML = this.value;
}*/



$('.range').on('change', function(){
    var that = $(this);
    that.prev('p').find('span').text(that.val());
});

$('.range-text').each(function(){
    var that = $(this);
    var val = that.parent().next('input').val();
    that.text(val)
});



});


const headers = document.querySelectorAll("[data-name='accordion-title']");

console.log("header",headers);

headers.forEach(function(item){
    item.addEventListener("click", headerClick);
});

function headerClick(){
    console.log("item", this.nextElementSibling);

    this.nextElementSibling.classList.toggle("accordion-body");

};

$(document).on('ready', function() {
        $('.carusle').slick({
            infinite:true,
            arrows:false,
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 6,
            responsive: [
            {
                infinite:true,
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 4
                }
            },
            {
                infinite:true,
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                infinite:true,
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                infinite:true,
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
            ]
        });
    });
