<?php
/** @var CMain $APPLICATION */

use \Bitrix\Iblock\ElementTable,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock as HL,
    Bitrix\Iblock\IblockTable;

Loader::includeModule('iblock');
Loader::includeModule('highloadblock');

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once __DIR__ . '/config-site.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/main/frontend-gulp/vendor/autoload.php';

function getElements(int $IBLOCK_ID, int $limit = 1000) {
//    $elements = CIBlockElement::GetList(["SORT" => "ASC"], [
//        'IBLOCK_ID' => $IBLOCK_ID
//    ], false, ["nTopCount" => $limit]);
//    $result = $elements->Fetch();
//

    $elements = ElementTable::getList(
        [
            'filter' => [
                'IBLOCK_ID' => $IBLOCK_ID,
//                'IBLOCK_ID' => 7,
                'ACTIVE' => 'Y'
            ],
            'select' => [
                'ID',
                'NAME',
                'CODE',
                'PREVIEW_TEXT',
                'PREVIEW_PICTURE'
            ],
            'limit' => $limit
        ]
    )->fetchAll();

    if ($elements && count($elements) > 0) {
        foreach ($elements as &$element) {
            $element['PREVIEW_PICTURE'] = CFile::GetFileArray($element["PREVIEW_PICTURE"]);
        }
    }

    return $elements;
}

function whoCall() {
    $file = \FrontendGulp\Api\Tools\Dump::whoCall(false, 2)['file'];

    return $file ? substr($file, strlen($_SERVER['DOCUMENT_ROOT'])) : '';
}

/**
 * @throws \Bitrix\Main\SystemException
 */
function includeBlock(string $path, array $params = []) {
    global $APPLICATION;
    $pathToFile = SITE_TEMPLATE_PATH . '/include/' . $path;

    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $pathToFile)) {
        throw new Exception("File: '$pathToFile' not exist");
    }

    return $APPLICATION->IncludeFile(
        $pathToFile,
        $params,
        [
            'NAME' => whoCall()
        ]
    );
}

function getListByHighload(int $highloadBlockId, array $params = []): \Bitrix\Main\ORM\Query\Result {
    $highloadBlock = HL\HighloadBlockTable::getById($highloadBlockId)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($highloadBlock);
    return ($entity->getDataClass())::getList($params);
}

function getTextByHighload(string $code): string {
    $textObject = getListByHighload(TEXT_CONSTANTS_HBLOCK_ID, [
        'filter' => ['UF_CODE' => $code]
    ])->Fetch();

    return $textObject ? $textObject['UF_TEXT'] : '';
}

function newsItemsToCardMenuItems(array $items = []): array {
    return array_map(function($item) {
        return array_merge($item, ['LINK' => $item['DETAIL_PAGE_URL']]);
    }, $items);
}

function getPartners() {
    $partners = ElementTable::getList(
        [
            'filter' => [
                'IBLOCK_ID' => 7,
                'ACTIVE' => 'Y'
            ],
            'select' => [
                'NAME',
                'PREVIEW_PICTURE'
            ]
        ]
    )->fetchAll();
    return $partners;
}

function getIblockIdByCode ($code = '' )
{
    if (!$code) {
        return 0;
    }

    return IblockTable::getList([
        'filter' => [
            'CODE' => $code
        ],
        'select' => [
            'ID'
        ]
    ])->fetchAll()[0]['ID'];
}

function getIblockTypeByCode ($code = '' )
{
    if (!$code) {
        return 0;
    }

    return IblockTable::getList([
        'filter' => [
            'CODE' => $code
        ],
        'select' => [
            'IBLOCK_TYPE_ID'
        ]
    ])->fetchAll()[0]['IBLOCK_TYPE_ID'];
}
