<?php /** @var array $steps */ ?>
<div class="catalog-page-doors">
    <div class="catalog-page-doors__header">
        <?php includeBlock('blocks/header.php'); ?>
    </div>

    <div class="catalog-page-doors__container">
        <header class="catalog-page-doors__header">
            <div class="catalog-page-doors__title">Каталог</div>

            <div class="catalog-page-doors__steps">
                <?php includeBlock('blocks/steps.php', ['steps' => $steps]); ?>
            </div>

            <div class="catalog-page-doors__promo">
                <div class="catalog-page-doors__promo-item">
                    <img class="catalog-page-doors__promo-item-img" src="/local/templates/doors-2.0/assets/img/exported/doors-furniture.png"/>
                </div>
                <div class="promo__item">
                    <img class="catalog-page-doors__promo-item-img" src="/local/templates/doors-2.0/assets/img/exported/drive.png"/>
                </div>
            </div>

            <div class="catalog-page-doors__hits">
                <span class="catalog-page-doors__hits-icon">ХИТЫ ПРОДАЖ!</span>

                Слайдер по отфильтрованным товарам
            </div>
        </header>

        <section class="catalog-container">
            <div class="catalog-list">
                <div class="catalog-item">

                </div>
            </div>
        </section>

    </div>
</div>
