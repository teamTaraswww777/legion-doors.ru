<?php

$benefits = getElements(BENEFITS_IBLOCK_ID, 3);
?>

<div class="home">
    <div class="home__container">
        <div class="home__content">
            <div class="home__benefits">
                <?php includeBlock('blocks/benefits.php', ['benefits' => $benefits]) ?>
            </div>

            <div class="home__info"
                 style="background-image: url(<?= SITE_TEMPLATE_PATH . '/assets/img/main-bg-door.png' ?>)">
                <div class="home__title">
                    <a class="home__link" href="/catalog">
                        <?= getTextByHighload(TEXT_MAIN_PAGE_TITLE) ?>
                    </a>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/logo-white.png"
                         alt=""
                         title=""
                         class="home__logo-img">
                </div>
            </div>
        </div>
    </div>
</div>
