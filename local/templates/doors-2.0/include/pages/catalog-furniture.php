<div class="catalog-page">
    <div class="catalog-page__header">
        <? includeBlock('blocks/header.php');?>
    </div>

    <div class="catalog-page__container">
        <div class="catalog-page__title">Каталог</div>
        <div class="catalog-page__menu">
            <div class="card-menu">
                <a class="card-menu__item" href="/catalog/doors">
                    <div class="card-menu__item-text">Двери</div>
                </a>
                <a class="card-menu__item" href="/catalog/furniture">
                    <div class="card-menu__item-text">Фурнитура</div>
                </a>
                <a class="card-menu__item" href="/catalog/gates">
                    <div class="card-menu__item-text">Гаражные ворота</div>
                </a>
            </div>
        </div>
    </div>
</div>
