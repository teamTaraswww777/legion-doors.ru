<?php
/** @var array $arResult */
?>
<? if ($arResult['ITEMS']): ?>
    <section class="our-partners">
        <? if (isset($arResult['NAME'])): ?>
            <h2 class="our-partners__title"><?= $arResult['NAME'] ?></h2>
        <? endif; ?>
        <div class="our-partners__list">
            <? foreach ($arResult['ITEMS'] as $item): ?>
                <div class="our-partners__item">
                    <img class="our-partners__img"
                         src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
                         alt="<?= $item['PREVIEW_PICTURE']['ALT'] ?>"
                         title="<?= $item['PREVIEW_PICTURE']['TITLE'] ?>"
                    >
                </div>
            <? endforeach; ?>
        </div>
    </section>
<? endif; ?>
