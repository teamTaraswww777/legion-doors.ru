<?php /** @var CBitrixComponentTemplate $componentTemplate */ ?>
<?php if (isset($items)): ?>
    <div class="card-menu">
        <?php foreach ($items as $item): ?>
            <?php if (isset($componentTemplate)) {
                $componentTemplate->AddEditAction($items['ID'], $items['EDIT_LINK'], CIBlock::GetArrayByID($items["IBLOCK_ID"], "ELEMENT_EDIT"));
                $componentTemplate->AddDeleteAction($items['ID'], $items['DELETE_LINK'], CIBlock::GetArrayByID($items["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
            } ?>
            <a class="card-menu__item" href="<?= $item['LINK'] ?>"
               id="<?= $componentTemplate ? $componentTemplate->GetEditAreaId($item['ID']) : ''; ?>">
                <div class="card-menu__item-text"><?= $item['NAME'] ?></div>
            </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
