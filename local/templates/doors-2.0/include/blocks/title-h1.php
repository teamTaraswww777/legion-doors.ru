<?php
/** @var string $img */
/** @var string $title */
?>

<div class="title-h1"
    <?php if ($img): ?>
        style="background-image: <?= $img ?>"
    <?php endif; ?>
>
    <h1 class="title-h1__text"><?= $title ?></h1>
</div>
