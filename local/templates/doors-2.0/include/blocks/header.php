<?php
/** @var CMain $APPLICATION */
?>
<div class="header">
    <div class="header__logo">
        <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/logo.png"/>
    </div>
    <div class="header__tagline">
        Ваш надежный барьер
    </div>
    <div class="header__contacts">
        <a class="header__contacts-phone" href="tel:8(495)664-66-03">8 (495) 664 66 03</a>
        <a class="header__contacts-email" href="mailto:prolegion-doors@yandex.ru">prolegion-doors@yandex.ru</a>
    </div>
    <div class="header__menu">
        <? $APPLICATION->IncludeComponent("bitrix:menu", "main-menu", array(
            "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
            "DELAY" => "N",    // Откладывать выполнение шаблона меню
            "MAX_LEVEL" => "1",    // Уровень вложенности меню
            "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                0 => "",
            ),
            "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
            "MENU_CACHE_TYPE" => "N",    // Тип кеширования
            "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
            "ROOT_MENU_TYPE" => "main",    // Тип меню для первого уровня
            "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
        ),
            false
        ); ?>
    </div>
</div>
