<?php
/** @var CBitrixComponentTemplate $componentTemplate */
/** @var array $params */
/** @var array $result */

?>

<div class="articles-list">
    <div class="articles-list__title">
        <?php includeBlock('blocks/title-h1.php', [
            'title' => $result['NAME']
        ]); ?>
    </div>
    <div class="articles-list__menu">
        <?php includeBlock('blocks/card-menu.php', [
            'items' => newsItemsToCardMenuItems($result['ITEMS']),
            'componentTemplate' => $componentTemplate
        ]); ?>
    </div>
</div>
