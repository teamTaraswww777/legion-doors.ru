<? if (!empty($menuResult)): ?>
    <div class="main-menu">
        <ul class="main-menu__list">
            <? foreach ($menuResult as $arItem): ?>
                <li class="main-menu__item <?= $arItem["SELECTED"] ? 'main-menu__item--selected' : '' ?> ">
                    <a class="main-menu__item-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </li>
            <? endforeach ?>
        </ul>
    </div>
<? endif ?>
