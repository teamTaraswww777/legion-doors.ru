<?php if (isset($steps)): ?>
    <div class="steps">
        <div class="steps__list">
            <?php foreach ($steps as $step): ?>
                <div class="steps__item">
                    <?= $step['name'] ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
