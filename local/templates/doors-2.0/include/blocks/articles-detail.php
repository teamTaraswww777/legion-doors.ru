<?php
/** @var CBitrixComponentTemplate $componentTemplate */
/** @var array $params */
/** @var array $result */

?>

<div class="articles-detail">
    <div class="articles-detail__title">
        <?php includeBlock('blocks/title-h1.php', [
            'title' => $result['NAME']
        ]); ?>
    </div>
    <div class="articles-detail__content">
        <?= $result['DETAIL_TEXT'] ?>
    </div>
</div>
