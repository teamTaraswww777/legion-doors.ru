<?
if (isset($benefits)): ?>
    <div class="benefits">
        <?php foreach ($benefits as $benefit): ?>
            <div class="benefits__item"
                 style="background-image: url(<?= $benefit['PREVIEW_PICTURE']['SRC'] ?>)">
                <div class="benefits__item-title"><?= $benefit['NAME'] ?></div>
                <div class="benefits__item-description"><?= $benefit['PREVIEW_TEXT'] ?></div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
