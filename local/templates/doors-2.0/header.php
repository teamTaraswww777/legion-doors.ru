<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;

?>

<!DOCTYPE html>
<html class="not_ie" lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $APPLICATION->ShowTitle() ?></title>
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH . '/assets/css/screen.css' ?>">
    <?php $APPLICATION->ShowHead(); ?>
    <?php includeBlock('metrics-head.php'); ?>
</head>

<body class="nojs">
<? $APPLICATION->ShowPanel() ?>
