<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if ($arResult['SECTIONS_COUNT'] > 0):?>
    <div class="catalog-page__container">
        <div class="catalog-page__title">Каталог</div>
        <div class="catalog-page__menu">
            <div class="card-menu">
                <?php foreach ($arResult['SECTIONS'] as $section): ?>
                    <a class="card-menu__item" href="<?= $section['SECTION_PAGE_URL'] ?>"
                       style="background-image: url('<?= $section['PICTURE']['SRC'] ?>')">
                        <div class="card-menu__item-text"><?= $section['NAME'] ?></div>
                    </a>
                <?php endforeach; ?>
                <!--
                                <a class="card-menu__item" href="/catalog/furniture">
                                    <div class="card-menu__item-text">Фурнитура</div>
                                </a>
                                <a class="card-menu__item" href="/catalog/gates">
                                    <div class="card-menu__item-text">Гаражные ворота</div>
                                </a>
                -->
            </div>
        </div>
    </div>
<?endif;?>