<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="benefits">
    <?php foreach ($arResult["ITEMS"] as $benefit): ?>

        <?
        $this->AddEditAction($benefit['ID'], $benefit['EDIT_LINK'], CIBlock::GetArrayByID($benefit["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($benefit['ID'], $benefit['DELETE_LINK'], CIBlock::GetArrayByID($benefit["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="benefits__item"
             style="background-image: url(<?= $benefit['PREVIEW_PICTURE']['SRC'] ?>)">
            <div class="benefits__item-title"><?= $benefit['NAME'] ?></div>
            <div class="benefits__item-description"><?= $benefit['PREVIEW_TEXT'] ?></div>
        </div>
    <? endforeach; ?>
</div>