<?php
use \Bitrix\Main\Loader;

Loader::includeModule('iblock');

$sectionsSource = CIBlockSection::GetList(
    [],
    [
        'IBLOCK_ID' => 5,
        'ACTIVE' => 'Y'
    ],
    false,
    [
        'ID',
        'IBLOCK_ID',
        'NAME',
        'UF_*'
    ]
);
$sectionsPrices = [];
while ($section = $sectionsSource->GetNext()) {
    $sectionsPrices[$section['ID']] = $section['UF_MIN_PRICE'];
}
foreach ($arResult['SECTIONS'] as $sectionNum => $section) {
    $arResult['SECTIONS'][$sectionNum]['UF_MIN_PRICE'] = $sectionsPrices[$section['ID']];
    if (intval($section['ELEMENT_CNT']) < 1) {
        unset($arResult['SECTIONS'][$sectionNum]);
    }
}
