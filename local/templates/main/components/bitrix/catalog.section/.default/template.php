<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if ($arResult['ITEMS']):?>
    <div class="sections-container">
        <?foreach ($arResult['ITEMS'] as $arItem):?>
            <div class="car">
                <div class="content">
                    <div class="cont-img">
                                <span>
                                    <a href="<?= $arItem['DETAIL_PAGE_URL']?>">
                                        <img class="preview-door-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                                    </a>
                                </span>
                    </div>
                    <div class="cont-info">
                        <div class="info-text">
                            <a href="<?= $arItem['DETAIL_PAGE_URL']?>">
                                <?= $arItem['NAME']?>
                            </a>
                        </div>
                        <div class="summ">
                                    <span>
                                        <?if (intval($arItem['PROPERTIES']['PRICE']['VALUE']) > 0):?>
                                            <?= $arItem['PROPERTIES']['SALE_PRICE']['VALUE'] ? $arItem['PROPERTIES']['SALE_PRICE']['VALUE'] : $arItem['PROPERTIES']['PRICE']['VALUE']?> Р
                                        <?else:?>
                                            Цена договорная
                                        <?endif;?>
                                    </span>
                            <?if ($arItem['PROPERTIES']['SALE_PRICE']['VALUE']):?>
                            <label class="old-price">
                                <?= $arItem['PROPERTIES']['PRICE']['VALUE']?> Р
                            </label>
                            <?endif;?>
                            <!--                                    <div class="icons">-->
                            <!--                                        <a href="#">-->
                            <!--                                            <img src="img/icons.png" alt="">-->
                            <!--                                        </a>-->
                            <!--                                    </div>-->
                        </div>
                    </div>
                    <div class="buttons">
                        <button data-fancybox data-src="#feedback" href="javascript:void();">Купить в один клик</button>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
    <?= $arResult['NAV_STRING']?>
<?endif;?>
<div class="contackt-form">
    <div class="container">
        <div class="forms">

            <form name="callback" action="<?= SITE_TEMPLATE_PATH?>/" method="post">
                <input type="hidden" name="popup" value="N">
                <div class="form-row">
                    <label for="name">Ваше имя*</label>
                    <input type="text" id="name" name="name" required>
                </div>
                <div class="form-row">
                    <label for="email">Ваш E-mail*</label>
                    <input type="email" id="email" name="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                </div>
                <div class="form-row">
                    <label for="phone">Ваш телефон*</label>
                    <input type="tel" placeholder="+7 (999) 999-99-99" id="phone" name="phone" data-inputmask="'mask': '+7 (999) 999-99-99'" required>
                </div>
                <div class="btn-container">
                    <input type="submit" class="send_form" name="submit_form" value="Вызвать замерщика" disabled="disabled"/>
                </div>
            </form>

        </div>
    </div>
</div>
