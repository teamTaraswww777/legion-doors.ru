<?php
if ($arResult['ITEMS']) {
    $itemsId = [];
    foreach ($arResult['ITEMS'] as $arItem) {
        $itemsId[] = $arItem['ID'];
    }
    $itemsSource = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => 5,
            'ID' => $itemsId
        ],
        false,
        false,
        [
            'ID',
            'IBLOCK_ID',
            'PROPERTY_PRICE',
            'PROPERTY_SALE_PRICE'
        ]
    );
    $items = [];
    $salePrices = [];
    while ($item = $itemsSource->GetNext()) {
        $items[$item['ID']] = $item['PROPERTY_PRICE_VALUE'];
        $salePrices[$item['ID']] = $item['PROPERTY_SALE_PRICE_VALUE'];
    }
    foreach ($arResult['ITEMS'] as $itemNum => $arItem) {
        $arResult['ITEMS'][$itemNum]['PROPERTIES']['PRICE']['VALUE'] = $items[$arItem['ID']];
        $arResult['ITEMS'][$itemNum]['PROPERTIES']['SALE_PRICE']['VALUE'] = $salePrices[$arItem['ID']];
    }
}