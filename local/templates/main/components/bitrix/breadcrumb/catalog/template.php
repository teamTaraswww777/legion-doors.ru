<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
    return "";

$strReturn = '';

$strReturn .= '<div class="filters">
                    <div class="container">
                        <div class="filters-inner">
                            <div class="filt-car">
                                <ul class="flex">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
    {
        $strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="'.$arResult[$index]["LINK"].'" itemprop="item">'
            .$title.
            '</a>
                            <p class="red" href="#"> > </p>
                        </li>';
    }
    else
    {
        $strReturn .= '
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			    <span class="current-link" itemprop="item">'
            .$title.
            '</span>
            </li>';
    }
}
$strReturn .= '</ul>
</div>
                </div>
                </div>
                </div>';


return $strReturn;