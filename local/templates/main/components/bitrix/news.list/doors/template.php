<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if ($arResult['SECTIONS']):?>
    <div class="slide-section">
        <?$num = 0;
        foreach ($arResult['SECTIONS'] as $section):?>
        <div class="<?if ($num === 0):?>section-top<?else:?>section-bottom<?endif;?>">
            <div class="container">
                <div class="eco">
                    <a>
                        <?= $section['NAME']?>
                    </a>
<!--                    <svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg">-->
<!--                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.5858 9.00001L0.292908 1.70712L1.70712 0.292908L10.4142 9.00001L1.70712 17.7071L0.292908 16.2929L7.5858 9.00001Z" fill="#2B2D42"/>-->
<!--                    </svg>-->

                </div>
            </div>
            <div class="section-slides  carusle">
                <?foreach ($section['ITEMS'] as $item):?>
                    <div class="car">
                        <div class="content">
                            <div class="cont-img">
                                <span>
                                    <a href="/catalog/<?= $section['CODE'] . '/' . $item['CODE']?>/">
                                        <img class="preview-door-img" src="<?= $item['PREVIEW_PICTURE']['SRC']?>" alt="">
                                    </a>
                                </span>
                            </div>
                            <div class="cont-info">
                                <div class="info-text">
                                    <a href="/catalog/<?= $section['CODE'] . '/' . $item['CODE']?>/">
                                        <?= $item['NAME']?>
                                    </a>
                                </div>
                                <div class="summ">
                                    <span>
                                        <?= $item['PROPERTIES']['PRICE']['VALUE']?> Р
                                    </span>
<!--                                    <div class="icons">-->
<!--                                        <a href="#">-->
<!--                                            <img src="img/icons.png" alt="">-->
<!--                                        </a>-->
<!--                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        </div>
        <?endforeach;?>
    </div>
<?endif;?>
