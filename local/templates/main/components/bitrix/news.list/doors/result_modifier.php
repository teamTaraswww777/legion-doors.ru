<?
use \Bitrix\Main\Loader,
    \Bitrix\Iblock\SectionTable;

Loader::includeModule('iblock');

if ($arResult['ITEMS']) {
    $sectionsId = [];
    $items = [];

    foreach ($arResult['ITEMS'] as $item) {
        if (!in_array($item['IBLOCK_SECTION_ID'], $sectionsId) && $item['IBLOCK_SECTION_ID']) {
            $sectionsId[] = intval($item['IBLOCK_SECTION_ID']);
        }
    }
    $sections = SectionTable::getList([
        'filter' => [
            'IBLOCK_ID' => 5,
            'ID' => $sectionsId
        ],
        'select' => [
            '*',
        ]
    ])->fetchAll();
    foreach ($sections as $section) {
        $arResult['SECTIONS'][$section['ID']]['NAME'] = $section['NAME'];
        $arResult['SECTIONS'][$section['ID']]['CODE'] = $section['CODE'];
    }
    foreach ($arResult['ITEMS'] as $item) {
        if ($item['IBLOCK_SECTION_ID']) {
            if (count( $arResult['SECTIONS'][$item['IBLOCK_SECTION_ID']]['ITEMS']) < 11) {
                $arResult['SECTIONS'][$item['IBLOCK_SECTION_ID']]['ITEMS'][] = $item;
            }
        }
    }
}