<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']): ?>
    <div class="intro-catalog">
        <label class="bg">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/Bg3.png" alt="">
        </label>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="slide  sliders">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <div class="slides">
                                <?if ($arItem['PROPERTIES']['IS_POPUP']['VALUE'] === 'Да'):?>
                                    <a class="full-block-link" data-fancybox data-src="#feedback" href="javascript:void();">
                                <?else:?>
                                    <?if ($arItem['PROPERTIES']['LINK']['VALUE']):?>
                                        <a class="full-block-link" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
                                    <?endif?>
                                <?endif;?>
                            <span>
                                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                            </span>
                                            <?if ($arItem['PROPERTIES']['IS_POPUP']['VALUE'] === 'Да' || $arItem['PROPERTIES']['LINK']['VALUE']):?>
                                </a>
                                        <?endif;?>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <label class="bg">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/Bg4.png" alt="">
        </label>
    </div>
<? endif; ?>
