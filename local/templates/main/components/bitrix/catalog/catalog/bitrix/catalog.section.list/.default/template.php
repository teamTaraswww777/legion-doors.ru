<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if ($arResult['SECTIONS_COUNT'] > 0):?>
    <?foreach ($arResult['SECTIONS'] as $arSection):
        dump($arSection)?>
        <div class="car">
            <div class="content">
                <div class="cont-img">
                                <span>
                                    <a href="<?= $arSection['SECTION_PAGE_URL']?>">
                                        <img class="preview-door-img" src="<?= $arSection['PICTURE']['SRC']?>" alt="">
                                    </a>
                                </span>
                </div>
                <div class="cont-info">
                    <div class="info-text">
                        <a href="<?= $arSection['SECTION_PAGE_URL']?>">
                            <?= $arSection['NAME']?>
                        </a>
                    </div>
                    <div class="summ">
                                    <span>
                                        <?= $item['PROPERTIES']['PRICE']['VALUE']?> Р
                                    </span>
                        <!--                                    <div class="icons">-->
                        <!--                                        <a href="#">-->
                        <!--                                            <img src="img/icons.png" alt="">-->
                        <!--                                        </a>-->
                        <!--                                    </div>-->
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
<?endif;?>