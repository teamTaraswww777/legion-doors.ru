<?php

$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(5);
$sectionData = $entity::getList(
    [
        'filter' => [
            'ID' => $arResult['IBLOCK_SECTION_ID']
        ],
        'select' => [
            'UF_GIFT'
        ]
    ]
)->fetchAll();
$arResult['GIFT'] = '';
if ($sectionData) {
    $arResult['GIFT'] = $sectionData[0]['UF_GIFT'];
}