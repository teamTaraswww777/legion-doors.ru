<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>
<div class="filters-bottom">

    <div class="container">

        <div class="row">


            <div class="filters-content">

                <div class="taabe"></div>

                <div class="col-lg-6 col-md-6 col-sm-5">

                    <div class="content-left">

							<span class="oun">
								<img src="<?= $arResult['DETAIL_PICTURE']['SRC']?>" alt="">
							</span>
<!--                        <span class="too">-->
<!--								<img src="img/download2.png" alt="">-->
<!--							</span>-->
                        <span class="free">
								<img src="<?= SITE_TEMPLATE_PATH?>/img/Ritsar2.png" alt="">
							</span>

                        <div class="content-function">
                            <?if ($arResult['GIFT']):?>
                            <div class="to-butt-headers">
                                <div class="container">
                                    <div class="row">
                                        <div class="to-butt-headers-car">
                                            <img src="<?= SITE_TEMPLATE_PATH?>/img/Free.png" alt="">
                                            <p class="text-botton"><?= $arResult['GIFT']?> в подарок!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?endif;?>
                            <div class="content-function-1">
									<span>
										<a href="#">
											<img src="<?= SITE_TEMPLATE_PATH?>/img/icon3.png" alt="">
										</a>
									</span>
                                <div class="content-function-text">
                                    <a class="text-top" href="#">
                                        Доставка и установка
                                    </a><br>
                                    <a class="text-botton" href="#">
                                        Бесплатно
                                    </a>
                                </div>

                            </div>
                            <div class="content-function-1">
									<span>
										<a href="#">
											<img src="<?= SITE_TEMPLATE_PATH?>/img/icon4.png" alt="">
										</a>
									</span>
                                <div class="content-function-text">
                                    <a class="text-top" href="#">
                                        Цвет окраса может быть любым
                                    </a><br>
                                    <a class="text-botton" href="#">
                                        любым по каталогу
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-7">


                    <div class="content-right">


<!--                        <a href="#">-->
<!--                            Отделка-->
<!--                        </a>-->

                        <h4>
                            <?= $arResult['NAME']?>
                        </h4>

<!--                        <span>-->
<!--								АРТИКУЛ 012.013-06.408-01.111-001.01-->
<!--							</span>-->


                        <div class="summa">

                            <label>
                                <?if (intval($arResult['PROPERTIES']['PRICE']['VALUE']) > 0):?>
                                    <?= $arResult['PROPERTIES']['SALE_PRICE']['VALUE'] ? $arResult['PROPERTIES']['SALE_PRICE']['VALUE'] : $arResult['PROPERTIES']['PRICE']['VALUE']?> ₽
                                <?else:?>
                                    Цена договорная
                                <?endif;?>
                            </label>
                            <?if ($arResult['PROPERTIES']['SALE_PRICE']['VALUE']):?>
                            <span>
									<?= $arResult['PROPERTIES']['PRICE']['VALUE']?> ₽
								</span>
                            <?endif;?>

                        </div>

                        <h5>
                            Размер двери
                        </h5>

                        <div class="input-rage-cont">


                            <div class="inp">
                                <p>Ширина:<span class="range-text"></span></p>
                                <input
                                    type="range"
                                    class="range"
                                    min="10"
                                    max="300"
                                    step="1"
                                    value="80"
                                >
                            </div>
                            <div class="inp">
                                <p>Высота:<span class="range-text"></span></p>
                                <input
                                    type="range"
                                    class="range"
                                    min="10"
                                    max="300"
                                    step="1"
                                    value="200"
                                >
                            </div>
                        </div>


<!--                        <div class="content-right-text-aleng list_group">-->
<!---->
<!--                            <div class="list_group_item header" data-name="accordion-title">-->
<!--                                <h6>-->
<!--                                    Конструкция - Базовая-->
<!--                                    <svg width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">-->
<!--                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M17.0968 0.292908L18.57 1.70712L9.50008 10.4142L0.430176 1.70712L1.90331 0.292908L9.50008 7.5858L17.0968 0.292908Z" fill="#EF233C"/>-->
<!--                                    </svg>-->
<!---->
<!--                                </h6>-->
<!--                            </div>-->
<!--                            <div class="list_group_item content accordion-body">-->
<!--                                <div class="content-right-text-aleng-left">-->
<!--										<span>-->
<!--											Толщина металла ......................... 2мм-->
<!--										</span>-->
<!--                                    <span>-->
<!--											Толщина полотна двери ........... 50мм-->
<!--										</span>-->
<!--                                </div>-->
<!--                                <div class="content-right-text-aleng-right">-->
<!--										<span>-->
<!--											Толщина металла ......................... 2мм-->
<!--										</span>-->
<!--                                    <span>-->
<!--											Толщина полотна двери ........... 50мм-->
<!--										</span>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="list_group_item header" data-name="accordion-title">-->
<!--								<span class="text-bott">-->
<!--									Все характеристики-->
<!--									<svg width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">-->
<!--										<path fill-rule="evenodd" clip-rule="evenodd" d="M17.0968 0.292908L18.57 1.70712L9.50008 10.4142L0.430176 1.70712L1.90331 0.292908L9.50008 7.5858L17.0968 0.292908Z" fill="#EF233C"/>-->
<!--									</svg>-->
<!---->
<!--								</span>-->
<!--                        </div>-->
<!--                        <div class="list_group_item content accordion-body">-->
<!--                            <div class="content-right-text-aleng-left">-->
<!--									<span>-->
<!--										Толщина металла ......................... 2мм-->
<!--									</span>-->
<!--                                <span>-->
<!--										Толщина полотна двери ........... 50мм-->
<!--									</span>-->
<!--                            </div>-->
<!--                            <div class="content-right-text-aleng-right">-->
<!--									<span>-->
<!--										Толщина металла ......................... 2мм-->
<!--									</span>-->
<!--                                <span>-->
<!--										Толщина полотна двери ........... 50мм-->
<!--									</span>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!---->
<!---->
                        <div class="row">

                            <div class="col-md-12  cooll">
                                <div class="color-picker">
<!--                                    <h6>-->
<!--                                        Запирающая система-->
<!--                                    </h6>-->
<!--                                    <div class="list-cont">-->
<!---->
<!--                                        <div class="list_group_item header" data-name="accordion-title">-->
<!---->
<!--                                            <div class="list-cont-logo">-->
<!--                                                <img src="img/799825cf8c6b8e29c87dc4460f654a9e.png" alt="">-->
<!--                                                <div class="list-cont-logo-info">-->
<!--                                                    <span>Базовая - 2 класс</span>-->
<!--                                                    <p>Border ЗВ8-8У + Border ЗВ4-3/55</p>-->
<!--                                                </div>-->
<!--                                                <div class="list-cont-logo-info-too">-->
<!--                                                    <svg width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">-->
<!--                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M17.0968 0.292908L18.57 1.70712L9.50008 10.4142L0.430176 1.70712L1.90331 0.292908L9.50008 7.5858L17.0968 0.292908Z" fill="#EF233C"/>-->
<!--                                                    </svg>-->
<!---->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                        <div class="paddin list_group_item content accordion-body">-->
<!--                                            <div class="content-right-text-aleng-left">-->
<!--													<span>-->
<!--														Толщина металла ......................... 2мм-->
<!--													</span>-->
<!--                                                <span>-->
<!--														Толщина полотна двери ........... 50мм-->
<!--													</span>-->
<!--                                            </div>-->
<!--                                            <div class="content-right-text-aleng-right">-->
<!--													<span>-->
<!--														Толщина металла ......................... 2мм-->
<!--													</span>-->
<!--                                                <span>-->
<!--														Толщина полотна двери ........... 50мм-->
<!--													</span>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!---->
<!---->
<!--                                    </div>-->





<!--                                    <span>-->
<!--											Цвет металлокаркаса:-->
<!--										</span>-->
<!---->
<!--                                    <div class="colorr-info">-->
<!---->
<!--                                        <div data-color="темно-синий" class="color-1"></div>-->
<!--                                        <div data-color="серый" class="color-2"></div>-->
<!--                                        <div data-color="белый" class="color-3"></div>-->
<!--                                        <div data-color="красный" class="color-4"></div>-->
<!---->
<!--                                    </div>-->

                                </div>

                                <div class="finish">


                                    <div class="finish-item">

                                        <div class="finish-external">
												<span class="check-finishing">
													Металлокаркас
												</span>
                                            <div class="finish-inner">

											<span>
												Цвет:
											</span>
                                                <div class="finish-color" data-place="frame">

                                                    <div data-color="серый" class="color-1"></div>
                                                    <div data-color="белый" class="color-2"></div>
                                                    <div data-color="темно-синий" class="color-3"></div>
                                                    <div data-color="красный" class="color-4"></div>
                                                    <div data-color="голубой" class="color-5"></div>

                                                </div>



                                            </div>
                                        </div>

                                        <div class="finish-external">
												<span class="check-finishing">
													Внешняя отделка
												</span>
                                            <div class="finish-inner">

											<span>
												Цвет:
											</span>
                                                <div class="finish-color" data-place="external">

                                                    <div data-color="серый" class="color-1"></div>
                                                    <div data-color="белый" class="color-2"></div>
                                                    <div data-color="темно-синий" class="color-3"></div>
                                                    <div data-color="красный" class="color-4"></div>
                                                    <div data-color="голубой" class="color-5"></div>

                                                </div>



                                            </div>
                                        </div>

                                        <div class="finish-external">
												<span class="check-finishing">
													Внутренняя отделка
												</span>
                                            <div class="finish-inner">

											<span>
												Цвет:
											</span>
                                                <div class="finish-color" data-place="inside">

                                                    <div data-color="серый" class="color-1"></div>
                                                    <div data-color="белый" class="color-2"></div>
                                                    <div data-color="темно-синий" class="color-3"></div>
                                                    <div data-color="красный" class="color-4"></div>
                                                    <div data-color="голубой" class="color-5"></div>

                                                </div>



                                            </div>
                                        </div>

                                    </div>



                                </div>


                                <div class="drawing">
<!---->
<!--										<span>-->
<!--											Рисунок:-->
<!--										</span>-->
<!---->
<!--                                    <div class="drawing-block">-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                        <div class="block-car">-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->

<!--                                    <p>-->
<!--                                        Цвет фурнитуры:-->
<!--                                    </p>-->
<!---->
<!--                                    <div class="drawing-color">-->
<!---->
<!--                                        <div class="color-1"></div>-->
<!--                                        <div class="color-2"></div>-->
<!---->
<!--                                    </div>-->

                                </div>


                            </div>

                        </div>

                        <div class="button form_call" data-fancybox data-src="#feedback" href="javascript:void();">Купить</div>
                    </div>


                </div>


            </div>


        </div>
        <div class="row pt-50 picture">
            <img src="<?= CFile::GetPath($arResult['PROPERTIES']['TECH_PICTURE']['VALUE']);?>">
        </div>
        <div class="header-butt">

            <div class="container">

                <div class="row">

                    <div class="col-sm-4">
                        <div class="content">

                            <span>
                                <img src="<?= SITE_TEMPLATE_PATH?>/img/sloy_4.png" alt="">
                            </span>
                            <label>
                                <img src="<?= SITE_TEMPLATE_PATH?>/img/sloy_1.png" alt="">
                            </label>

                            <div class="text">

                                <h2>
                                    Выезд замерщика - бесплатно
                                </h2>
                                <p>
                                    Вы можете прямо сейчас вызвать замерщика и он приедет, проведет консультацию и Вы сможете оформить заказ абсолютно бесплатно!
                                </p>

                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="content">

                            <span>
                                <img src="<?= SITE_TEMPLATE_PATH?>/img/sloy_5.png" alt="">
                            </span>
                            <label>
                                <img class="too" src="<?= SITE_TEMPLATE_PATH?>/img/sloy_2.png" alt="">
                            </label>

                            <div class="text">

                                <h2>
                                    Наши двери - это гарантии и качество
                                </h2>
                                <p>
                                    На все двери нашей компании действует гарантия сроком на 5 лет!
                                </p>

                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="content">

                            <span>
                                <img src="<?= SITE_TEMPLATE_PATH?>/img/sloy_6.png" alt="">
                            </span>
                            <label>
                                <img class="too" src="<?= SITE_TEMPLATE_PATH?>/img/sloy_3.png" alt="">
                            </label>

                            <div class="text">

                                <h2>
                                    Производство дверей по европейским стандартам
                                </h2>
                                <p>
                                    Для производства наших дверей мы используем передовые европейские технологии и оборудование. За качество отвечаем!
                                </p>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
<!--        <div class="doors-container pt-50">-->
<!--            <h1 class="title">Возможные формы двери</h1>-->
<!--            <ul>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/1.png">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/2.png">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/3.png">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/4.png">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/5.png">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <img src="--><?//= SITE_TEMPLATE_PATH?><!--/img/6.png">-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
    </div>
    <div class="contackt-form">
        <div class="container">
            <div class="forms">

                <form name="callback" action="<?= SITE_TEMPLATE_PATH?>/" method="post">
                    <input type="hidden" name="popup" value="N">
                    <input type="hidden" name="frame-color" value="">
                    <input type="hidden" name="inside-color" value="">
                    <input type="hidden" name="external-color" value="">
                    <div class="form-row">
                        <label for="name">Ваше имя*</label>
                        <input type="text" id="name" name="name" required>
                    </div>
                    <div class="form-row">
                        <label for="email">Ваш E-mail*</label>
                        <input type="email" id="email" name="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                    </div>
                    <div class="form-row">
                        <label for="phone">Ваш телефон*</label>
                        <input type="tel" placeholder="+7 (999) 999-99-99" id="phone" name="phone" data-inputmask="'mask': '+7 (999) 999-99-99'" required>
                    </div>
                    <div class="btn-container">
                        <input type="submit" class="send_form" name="submit_form" value="Вызвать замерщика" disabled="disabled"/>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>