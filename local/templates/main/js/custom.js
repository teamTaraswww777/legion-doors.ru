$(document).ready(function () {

	$(window).on("scroll load resize", function() {
		var introH = $('.header').innerHeight(),
			scrollPos = $(this).scrollTop();
		if(scrollPos > introH) {
			$('.header').addClass("fixed");
		} else {
			$('.header').removeClass("fixed");
		}

	});

	$('body').removeClass('nojs');
	$("img, a").on("dragstart", function (event) {
		event.preventDefault();
	});
	$('.range').rangeslider({
		polyfill: false,
	});

	$(".toggle-mnu").click(function () {
		$(this).toggleClass("on");
		$(".nav").slideToggle();
		return false;
	});

	$(".sliders").slick({
		lazyLoad: 'ondemand', // ondemand progressive anticipated
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
	});


	/*var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
        output.innerHTML = this.value;
    };

    var slideCol = document.getElementById("Range");
    var y = document.getElementById("demee");
    y.innerHTML = slideCol.value;

    slideCol.oninput = function() {
        y.innerHTML = this.value;
    }*/


	$('.range').on('change', function () {
		var that = $(this);
		that.prev('p').find('span').text(that.val());
	});

	$('.range-text').each(function () {
		var that = $(this);
		var val = that.parent().next('input').val();
		that.text(val)
	});


});


const headers = document.querySelectorAll("[data-name='accordion-title']");

console.log("header", headers);

headers.forEach(function (item) {
	item.addEventListener("click", headerClick);
});

function headerClick() {
	console.log("item", this.nextElementSibling);

	this.nextElementSibling.classList.toggle("accordion-body");

};

$('.carusle').slick({
	infinite: true,
	arrows: false,
	centerMode: true,
	centerPadding: '60px',
	slidesToShow: 6,
	responsive: [
		{
			infinite: true,
			breakpoint: 1200,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 4
			}
		},
		{
			infinite: true,
			breakpoint: 1024,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 3
			}
		},
		{
			infinite: true,
			breakpoint: 768,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 3
			}
		},
		{
			infinite: true,
			breakpoint: 480,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
			}
		}
	]
	});
$(document).ready(function() {
	$(document).on('click', '.form_call', function () {
		$(document).find('#feedback form input[name=frame-color]').val($(this).attr('data-frame'));
		$(document).find('#feedback form input[name=inside-color]').val($(this).attr('data-inside'));
		$(document).find('#feedback form input[name=external-color]').val($(this).attr('data-external'));
	})

	$(document).on('click', 'input[name=submit_form],button.send_form', function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		$.ajax({
			url: '/local/templates/main/ajax/sendMail.php',
			method: 'POST',
			data: form.serialize(),
			success: function(data) {
				if (form.find('input[name=popup]').val() === 'Y') {
					form.parents('#feedback').html('<h1 class="success">Ожидайте! Скоро с Вами свяжутся.</h1>');
					setTimeout(function () {
						$.fancybox.close();
						yaCounter66096808.reachGoal('PURCHASE')
					}, 3500)
				} else {
					form.parents('.contackt-form').html('<h1 class="success">Ожидайте! Скоро с Вами свяжутся.</h1>');
				}
			}
		})
	})

	$('input[name=phone]').inputmask('+7 (999) 999-99-99');

	$('form[name=callback] input:not([type=submit])').on('input', function () {
		var fields = 0;
		$('form[name=callback] input:not([type=submit])').each(function(index, input) {
			if($(input).val().length > 0) {
				fields++;
			}
		})
		console.log(fields);
		console.log($(this).parents('form').find('input[name=popup]').val());
		if ($(this).parents('form').find('input[name=popup]').val() === 'Y') {
			if (fields === 5) {
				$(document).find('input[name=submit_form]').attr('disabled', false);
			} else {
				$(document).find('input[name=submit_form]').attr('disabled', true);
			}
		} else {
			if (fields === 5) {
				$(document).find('input.send_form').attr('disabled', false);
			} else {
				$(document).find('input.send_form').attr('disabled', true);
			}
		}
	})
	$('.finish-color div').on('click', function(){
		$(this).parents('.finish-color').find('div').each(function(index, value) {
			$(this).removeClass('active');
		})
		$(this).addClass('active');
		var place = $(this).parents('.finish-color').data('place');
		$(document).find('.button.form_call').attr('data-' + place, $(this).attr('data-color'));
	})

	$(document).on('click', '#door-2', function () {
		window.location.href = $(this).attr('href');
	})
});