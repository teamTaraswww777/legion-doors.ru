<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;

Loader::includeModule('main');

$res = CEvent::Send(
    'CALL_BACK',
    's1',
    [
        'NAME' => $_REQUEST['name'],
        'PHONE' => $_REQUEST['phone'],
        'EMAIL' => $_REQUEST['email'],
    ]
);
?>
