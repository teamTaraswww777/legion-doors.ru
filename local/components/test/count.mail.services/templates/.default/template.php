<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if ($arResult['MAIL_SERVICES']):?>
    <table>
        <tbody>
            <?foreach ($arResult['MAIL_SERVICES'] as $mailService => $ordersNumber):?>
                <tr>
                    <td><?= $mailService?></td>
                    <td><?= $ordersNumber?></td>
                </tr>
            <?endforeach;?>
        </tbody>
    </table>
<?endif;?>
