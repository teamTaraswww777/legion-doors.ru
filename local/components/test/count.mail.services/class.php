<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

use Bitrix\Highloadblock as HL,
    \Bitrix\Main\Entity,
    \Bitrix\Main\SystemException,
    \Bitrix\Main\Loader;

Loader::includeModule('highloadblock');

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 */

class MailServicesCounter extends CBitrixComponent
{

    private $testTableID = 1;

    private function getUserAccess ()
    {
        global $USER;
        if ($USER->IsAdmin()) {
            return true;
        } else {
            return false;
        }
    }

    private function getHlTableDataClass ()
    {
        $hlblock = HL\HighloadBlockTable::getById($this->testTableID)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        return $entityClass;
    }

    private function getMailServices ()
    {
        $mailServices = [];
        $entityClass = $this->getHlTableDataClass();
        $orderMails = $entityClass::getList(
            [
                'select' => [
                    'UF_MAIL'
                ]
            ]
        )->fetchAll();
        foreach ($orderMails as $mail) {
            $service = explode('@', $mail['UF_MAIL'])[count(explode('@', $mail['UF_MAIL'])) - 1];
            if (!in_array($service, $mailServices)) {
                $mailServices[] = $service;
            }
        }
        return $mailServices;
    }

    private function getServicesCount ()
    {
        $mailServices = $this->getMailServices();
        $entityClass = $this->getHlTableDataClass();
        foreach ($mailServices as $mailService) {
            $serviceCount = $entityClass::getList(
                [
                    'filter' => [
                        '%UF_MAIL' => '@' . $mailService
                    ],
                    'select' => [
                        'UF_MAIL'
                    ],
                    'count_total' => 1
                ]
            )->getCount();
            $servicesCount[$mailService] = $serviceCount;
        }
        return $servicesCount;
    }

    public function executeComponent ()
    {
        try
        {
            if (!$this->getUserAccess())
                throw new SystemException("Доступно только для администратора");

            if($this->startResultCache())
            {
                $this->arResult["MAIL_SERVICES"] = $this->getServicesCount();
                $this->includeComponentTemplate();
            }
            return $this->arResult["MAILS"];
        }
        catch (SystemException $exception)
        {
            echo $exception->getMessage();
        }
    }
}